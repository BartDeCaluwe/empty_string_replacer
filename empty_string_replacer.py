#!/usr/bin/env python3
import json
import os

folder = './src/assets/i18n/'

# Loop over translation files
for filename in os.listdir(folder):
    with open(folder + filename, 'r') as file:
        try:
            filedata = json.load(file)
        except json.JSONDecodeError as error:
            print('Failed to decode file: ' + error.msg)
            sys.exit()

    # Replace empty string value with none
    for line in filedata:
        if filedata[line] == "":
          filedata[line] = None

    with open(folder + filename, 'w') as file:
        json.dump(filedata, file,  indent=2, ensure_ascii=False)
        # Add empty line as dumping json removes it
        file.write('\n')
